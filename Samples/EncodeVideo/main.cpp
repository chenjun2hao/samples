/**
 * ============================================================================
 *
 * Copyright (C) 2019, Huawei Technologies Co., Ltd. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1 Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   2 Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   3 Neither the names of the copyright holders nor the names of the
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * ============================================================================
 */

#include "main.h"
#include <unistd.h>
#include <libgen.h>
#include <cstring>
#include <sstream>
#include <string>
#include <fstream>
#include <cstdlib>
#include <sys/types.h>
#include <sys/wait.h>
#include "hiaiengine/api.h"
#include "hiaiengine/data_type.h"
#include "hiaiengine/data_type_reg.h"
#include "FileManager.h"
#include "Common.h"
#include "DataType.h"

const int CHIP_ID = 0;
const uint32_t GRAPH_ID = 442;
const std::string GRAPH_CONFIG = "./graph.config";
const uint32_t USLEEP_TIME = 100000;
const uint32_t TARGET_ENGINE_ID = 203;
const uint32_t SRC_ENGINE_ID = 673;

const char HELP_MESSAGE[] = "Print a usage message.";
const char I_MESSAGE[] =
    "Requested. Specify the input image folder in which 0.yuv, 1.yuv, ...  is saved, eg, /home/data/";
const char FRAME_NUM_MESSAGE[] =
    "Requested. Specify the image number in the folder specified by pararmeter -i";
const char HEIGHT_MESSAGE[] = "Requested. Specify the height of input image range = [128, 1920]";
const char WIDTH_MESSAGE[] = "Requested. Specify the width of input image range = [128, 1920]";
const char IMAGE_TYPE_MESSAGE[] =
    "Optional. Specify the format of input yuv file, 0: YUV420SP_UV, 1: YUV420SP_VU(default)";
const char ENCODE_TYPE_MESSAGE[] =
    "Optional. Specify the encode type, 0: H265, 1: H264B(default), 2: H264M, 3: H264H";
const uint32_t MAX_FOLDER_NAME = 256;
const uint32_t SLEEP_1_MS = 1000;

static int32_t g_flag = 0;

#define CHANGE_DIR_TO_CURRENT_DIR()                                  \
    do {                                                             \
        std::shared_ptr<FileManager> fileManager(new FileManager()); \
        string path(argv[0], argv[0] + strlen(argv[0]));             \
        fileManager->ChangeDir(path.c_str());                        \
    } while (0)

HIAI_REGISTER_DATA_TYPE("EncodeVideoBlock", EncodeVideoBlock);
/**
 * @ingroup FasterRcnnDataRecvInterface
 * @brief RecvData 
 */
HIAI_StatusT CustomDataRecvInterface::RecvData(const std::shared_ptr<void> &message)
{
    std::shared_ptr<std::string> data = std::static_pointer_cast<std::string>(message);
    g_flag--;

    return HIAI_OK;
}

// Init and create graph
HIAI_StatusT HIAI_InitAndStartGraph(int chipIndex, uint32_t GRAPH_ID, std::string graphConfig,
                                    hiai::EnginePortID target_port_config)
{
    // Step1: Global System Initialization before using HIAI Engine
    HIAI_StatusT status = HIAI_Init(chipIndex);
    if (status != HIAI_OK) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "Failed to start the graph");
        return status;
    }
    // Step2: Create and Start the Graph
    status = hiai::Graph::CreateGraph(graphConfig);
    if (status != HIAI_OK) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "Failed to start the graph");
        return status;
    }

    // Step3: Get the Graph
    std::shared_ptr<hiai::Graph> graph = hiai::Graph::GetInstance(GRAPH_ID);
    if (graph == nullptr) {
        HIAI_ENGINE_LOG("Failed to get the graph-%u", GRAPH_ID);
        return status;
    }

    graph->SetDataRecvFunctor(target_port_config,
                              std::shared_ptr<CustomDataRecvInterface>(new CustomDataRecvInterface("")));

    return HIAI_OK;
}

static HIAI_StatusT CheckArgs(const std::string &folderName, const int frameNum, EncodeVideoBlock encodeConfig)
{
    const int HEIGHT_WIDTH_MIN = 128;
    const int HEIGHT_WIDTH_MAX = 1920;
    const int ENCODE_TYPE_MAX = 3;
    // check if the file exist
    shared_ptr<FileManager> fileManager(new FileManager());
    if (fileManager->ExistDir(folderName) == false) {
        printf("[ERROR] Input file %s doesn't exist or It is a file. Please check!\n", folderName.c_str());
        return HIAI_ERROR;
    }
    if (frameNum <= 0) {
        printf("[ERROR] the frameNum is not valid or given, please check the help message\n");
        return HIAI_ERROR;
    }
    if (encodeConfig.imageWidth < HEIGHT_WIDTH_MIN || encodeConfig.imageWidth > HEIGHT_WIDTH_MAX ||
        encodeConfig.imageHeight < HEIGHT_WIDTH_MIN || encodeConfig.imageHeight > HEIGHT_WIDTH_MAX) {
        printf("[ERROR] the height or width is not valid or given, please check the help message\n");
        return HIAI_ERROR;
    }
    /* YUV file format: 0: YUV420SP_UV, 1: YUV420SP_VU(default) */
    if (encodeConfig.imageType < 0 || encodeConfig.imageType > 1) {
        printf("[ERROR] the imageType is not valid or given, please check the help message\n");
        return HIAI_ERROR;
    }
    /* Encode type: 0: H265, 1: H264B(default), 2: H264M, 3: H264H */
    if (encodeConfig.encodeType < 0 || encodeConfig.encodeType > ENCODE_TYPE_MAX) {
        printf("[ERROR] the encodeType is not valid or given, please check the help message\n");
        return HIAI_ERROR;
    }

    return HIAI_OK;
}

static void ShowUsage()
{
    printf("\nUsage: ./main [Options...]\n\n");
    printf("Options:\n");
    printf("    -h                             %s\n", HELP_MESSAGE);
    printf("    -i <path>                      %s\n", I_MESSAGE);
    printf("    -frameNum <num>                %s\n", FRAME_NUM_MESSAGE);
    printf("    -height <num>                  %s\n", HEIGHT_MESSAGE);
    printf("    -width  <num>                  %s\n", WIDTH_MESSAGE);
    printf("    -imageType <num>               %s\n", IMAGE_TYPE_MESSAGE);
    printf("    -encodeType <num>              %s\n\n", ENCODE_TYPE_MESSAGE);
    printf("example:\n");
    printf("./main -i /home/HwHiAiUser/data/ -frameNum 100 -height 224 -width 224 -imageType 1 -encodeType 0\n\n");
}

void ParserInputPara(int argc, char *argv[], CommandParser &options, EncodeVideoBlock &encodeConfig)
{
    options.addOption("-h")
    .addOption("-i", "../data/test.yuv")
    .addOption("-height", "0")
    .addOption("-width", "0")
    .addOption("-frameNum", "0")
    .addOption("-imageType", "1")
    .addOption("-encodeType", "1");
    options.parseArgs(argc, argv);

    bool help = options.cmdOptionExists("-h");
    std::string folderName = options.cmdGetOption("-i");
    int frameNum = parseStrToInt(options.cmdGetOption("-frameNum"));
    encodeConfig.imageHeight = parseStrToInt(options.cmdGetOption("-height"));
    encodeConfig.imageWidth = parseStrToInt(options.cmdGetOption("-width"));
    /* YUV file format: 0: YUV420SP_UV, 1: YUV420SP_VU(default) */
    encodeConfig.imageType = parseStrToInt(options.cmdGetOption("-imageType"));
    /* Encode type: 0: H265, 1: H264B(default), 2: H264M, 3: H264H */
    encodeConfig.encodeType = parseStrToInt(options.cmdGetOption("-encodeType"));
    // check the validity of input argument
    if (help || CheckArgs(folderName, frameNum, encodeConfig) != HIAI_OK) {
        ShowUsage();
        exit(1);
    }
}

int SetPort(uint32_t graphID, hiai::EnginePortID &srcPortConfig, hiai::EnginePortID &targetPortConfig)
{
    srcPortConfig.graph_id = graphID;
    srcPortConfig.engine_id = SRC_ENGINE_ID;
    srcPortConfig.port_id = 0;

    targetPortConfig.graph_id = graphID;
    targetPortConfig.engine_id = TARGET_ENGINE_ID;
    targetPortConfig.port_id = 0;

    return 0;
}

int main(int argc, char *argv[])
{
    CommandParser options;
    EncodeVideoBlock encodeConfig;
    ParserInputPara(argc, argv, options, encodeConfig);
    std::string folderName = options.cmdGetOption("-i");
    int frameNum = parseStrToInt(options.cmdGetOption("-frameNum"));

    CHANGE_DIR_TO_CURRENT_DIR();

    hiai::EnginePortID targetPortConfig;
    hiai::EnginePortID srcPortConfig;
    SetPort(GRAPH_ID, srcPortConfig, targetPortConfig);

    // 1.create graph
    HIAI_StatusT ret = HIAI_InitAndStartGraph(CHIP_ID, GRAPH_ID, GRAPH_CONFIG, targetPortConfig);
    if (ret != HIAI_OK) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "Failed to start graph");
        return HIAI_ERROR;
    }

    // 2.send data
    std::shared_ptr<hiai::Graph> graph = hiai::Graph::GetInstance(GRAPH_ID);
    if (graph == nullptr) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "Failed to get the graph-%u", GRAPH_ID);
        return HIAI_ERROR;
    }

    /* The output frame number = frameNum + one header frame */
    g_flag = frameNum + 1;
    // send data to SourceEngine 0 port
    for (int i = 0; i < frameNum; i++) {
        std::shared_ptr<EncodeVideoBlock> dataPtr = std::make_shared<EncodeVideoBlock>();
        *dataPtr = encodeConfig;
        char tmp[MAX_FOLDER_NAME];
        if (sprintf_s(tmp, MAX_FOLDER_NAME, "%d.yuv", i) < 0) {
            HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[main] sprintf filename error!");
            return HIAI_ERROR;
        }
        dataPtr->rawImageFile = folderName + string(tmp);
        dataPtr->frameId = i;
        dataPtr->isEOS = (i < (frameNum - 1)) ? 0 : 1;
        if (HIAI_OK != graph->SendData(srcPortConfig, "EncodeVideoBlock", std::static_pointer_cast<void>(dataPtr))) {
            HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[main] graph->SendData() error!");
            return HIAI_ERROR;
        }
        usleep(SLEEP_1_MS);
    }

    while (g_flag > 0) {
        usleep(USLEEP_TIME);
    }
    hiai::Graph::DestroyGraph(GRAPH_ID);
    std::cout << "Video Encode success!" << std::endl;

    return 0;
}
