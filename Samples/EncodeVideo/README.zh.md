中文|[英文](README.md)
## 介绍

本开发样例演示EncodeVideo程序，主要演示yuv文件编码生成.bin文件的流程。
流程框架

    EncodeVideo: main(Host) > VideoIn(Host) > VideoEncode(Device) > VideoOut(Host)

## 支持的产品

Atlas 800 (Model 3000), Atlas 300 (Model 3010), Atlas 500 (Model 3010)

## 支持的版本

1.3.T33.B890 1.3.2.B893 1.3.5.B896 1.31.T12.B120 1.31.T15.B150 1.32.T7.B070

版本号查询方法，在Atlas产品环境下，运行以下命令：
```bash
npu-smi info
```

## 编译

编译Atlas 800 (Model 3000) 或 Atlas 300 (Model 3010)程序
```bash
./build.sh A300
```

编译Atlas 500 (Model 3010)程序
```bash
./build.sh A500
```

备注: 编译脚本build.sh默认运行参数为"A300"，运行以下命令，将默认编译Atlas300程序。
```bash
./build.sh
```

## 运行
Sample默认输入为连续的yuv格式图片, 命名为0.yuv, 1.yuv, 2.yuv ... 要求从0开始，顺序增加。 请将图片保存到同一个目录下，通过参数[-i]设置该目录。
查看帮助文档
```bash
cd out
./main -h
```
    -h                             Print a usage message.
    -i <path>                      Requested. Specify the input image folder in which 0.yuv, 1.yuv, ...  is saved, eg, /home/data/
    -frameNum <num>                Requested. Specify the image number in the folder specified by pararmeter -i
    -height <num>                  Requested. Specify the height of input image range = [128, 1920]
    -width  <num>                  Requested. Specify the width of input image range = [128, 1920]
    -imageType <num>               Optional. Specify the format of input yuv file, 0: YUV420SP_UV, 1: YUV420SP_VU(default)
    -encodeType <num>              Optional. Specify the encode type, 0: H265, 1: H264B(default), 2: H264M, 3: H264H

```bash
cd out
./main -i /home/HwHiAiUser/data/ -frameNum 100 -height 224 -width 224 
```
备注：
1、本例程的输入yuv图片，可采用DecodeVideo的输出图片作为输入，修改main.cpp的读取文件的相关代码即可;
2、本例程生成的视频流文件venc.bin, 可以用VLC或ffmpeg播放; 