EN|[CN](README.zh.md)
## Introduction

This sample demonstrates how to use the EncodeVideo program to encode a YUV file into a .bin file.
Process Framework

    EncodeVideo: main(Host) > VideoIn(Host) > VideoEncode(Device) > VideoOut(Host)

## Supported Products

Atlas 800 (Model 3000), Atlas 300 (Model 3010), Atlas 500 (Model 3010)

## Supported Version

1.3.T33.B890 1.3.2.B893 1.3.5.B896 1.31.T12.B120 1.31.T15.B150 1.32.T7.B070

Run the following command to check the version in the environment where the Atlas product is installed:
```bash
npu-smi info
```

## Compilation

Compile the Atlas 800 (Model 3000) or Atlas 300 (Model 3010) program:
```bash
./build.sh A300
```

Compile the Atlas 500 (Model 3010) program:
```bash
./build.sh A500
```

Note: the default run parameter of the compile script build.sh is "A300". It will compile Atlas300 program when running following command:
```bash
./build.sh
```

## Execution
The input YUV images under target path are required to be named as '0.yuv','1.yuv',etc. which should keep in order and START FROM 0. The path is set by option [-i].
View the help document:
```bash
cd out
./main -h
```
    -h                             Print a usage message.
    -i <path>                      Requested. Specify the input image folder in which 0.yuv, 1.yuv, ...  is saved, eg, /home/data/
    -frameNum <num>                Requested. Specify the image number in the folder specified by pararmeter -i
    -height <num>                  Requested. Specify the height of input image range = [128, 1920]
    -width  <num>                  Requested. Specify the width of input image range = [128, 1920]
    -imageType <num>               Optional. Specify the format of input yuv file, 0: YUV420SP_UV, 1: YUV420SP_VU(default)
    -encodeType <num>              Optional. Specify the encode type, 0: H265, 1: H264B(default), 2: H264M, 3: H264H

```bash
cd out
./main -i /home/HwHiAiUser/data/ -frameNum 100 -height 224 -width 224 
```
Note:
1.We can take the output images of DecodeVideo as the input of this Sample; Just remember to alter the file reading part in main.cpp;
2.This Sample will generate a stream file venc.bin which could be played by VLC or ffmpeg;