英文|[说明](README.md)

# Atlas500 SSD/YOLOV3/Centerface object detection


## 1. 简介  

    fork from 原项目，主要用于做atlas硬件平台的目标检测算法测试和移植。成功移植了[centerface](https://github.com/chenjun2hao/CenterFace.pytorch)

    
## 2. requirements

- Atlas 500 (Model 3010)
- ubuntu16.04

## 3. 版本信息

```bash
npu-smi info
```
{
    "VERSION": "1.3.11.B902", 
    "NAME": "DDK", 
    "TARGET": "ASIC"
}
注意： 如果版本不同，需要重新转换模型与编译项目

## 4. 操作说明

详情可参考:[博客](https://blog.csdn.net/u011622208/article/details/107019169)

## 5. 使用

### 5.1 编译

```bash
cd $Samples/InferObjectDetection
bash ./build.sh A500
```

### 5.2 推理

将编译生成的out文件上传到atlas硬件平台上，按以下命令说明进行执行。


```bash
cd out
./ObjectDetection -h

ObjectDetection [OPTION]
Options:

    -h                             Print a usage message.
    -i '<path>'                    Optional. Specify the input file: jpg iamge or H264 Video, default: test.jpeg
    -g '<graph file>'              Optional. Specify the graph config file, default: graph.config
    -m '<model file>'              Optional. Specify the model file, default: load from graph.config
    -c '<chanel number in graph>'  Optional. Specify the number of video channle in one graph, range [1, 4], default: 1
    -s '<device id>'               start chip id, default is 0
    -e '<device id>'               end chip id, default is 0
    -t '<model id>'                choose the detection type, 0: SSD, 1: YOLOV3, 2：centerface default is 1

Eg: 
- yolov3安全帽检测
`./ObjectDetection -i ./000009.jpg -t 1 -m ./yolov3_helmet.om -g ./graph.config -s 0 -e 0`

- centerface安全帽检测
`./ObjectDetection -i ./000009.jpg -t 2 -m ./center_c.om -g ./graph.config -s 0 -e 0`