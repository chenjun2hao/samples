EN|[CN](README.zh.md)

# SSD/YOLOv3 ObjectDection Sample


## Introduction

This Sample demonstrate how to implement the following procedure.

    Take H264/JPG as input -> decode the video/image -> data format transformation -> ObjectDetection -> Output the results

These APIs are strongly involved in this Sample: VDEC, JPEGE, AIModelManager etc.

## Supported Products

Atlas 300 (Model 3000), Atlas 300 (Model 3010), Atlas 500 (Model 3010)

## Supported Version

1.3.T33.B890 1.3.2.B893 1.31.T12.B120 1.31.T15.B150 1.32.T7.B070

You may run the following command to get the currently installed version of Atlas product in the environment:
```bash
npu-smi info
```

## Dependency

1. Single input supported, the ouput layer would be the SSDDetection or YOLOv3 ObjectDetection model, like VGG-SSD, resnet-SSD, YOLOv3 etc, other models may requires different implementation in this sample.
2. JPEG in Huffman Coding is supproted; But th following codings are not: Arithmetic Coding, Progressive Coding and JPEG 2000.
3. This sample supoorts 264 format video input, you can use ffmpeg to tranform other file into h264 file like the following:
```bash
    /*-bf number of B Frame, -g internal between key frames, -s resolution, -an close the sound track, -r frame rate setting*/
    ffmpeg -i test.mp4 -vcodec h264 -bf 0 -g 25 -r 10 -s 1280*720 -an -f h264 test1.264
```
## Model transformation

refer to [model transformation instructions](data/models/README.md), download and convert related models.

## Configuration

configure path information in 'graph.config'
```bash
model path config
```
  engines {
    id: 103
    engine_name: "SSDDetection"
    ...
    ai_config {
      items {
        name: "model"
        value: "../data/models/vgg_ssd_300x300.om" # change to your model path
      }
    ...
  }

```bash
Modify code interface
```

The model running in this example is a training model for darknet based on 80 coco data sets. If you run your own model,
Plese modify these code in InferObjectDetection/ObjectDetectionEngine/ObjectDetectionEngine.h：
```bash
const int CLASS_NUM = 80;  // Change to the number of objects in your model
const int BIASES_NUM = 18;  // Change to the anchor size of your model
// Change to the anchors of your model
static float g_biases[BIASES_NUM] = {10, 13, 16, 30, 33, 23, 30, 61, 62,  45, 59, 119, 116, 90, 156, 198, 373, 326};  
```

## compilation

Compile this sample as Atlas300 (Model 3000) or Atlas 300 (Model 3010) program:
```bash
export DDK_HOME= <Atlas 300 model 300* DDK_HOME>/ddk
cd <Project Root Folder>
bash build.sh A300
```

Compile this sample as Atlas 500 (Model 3010) program:
```bash
export DDK_HOME= <Atlas 500 DDK_HOME>/ddk
cd <Project Root Folder>
bash build.sh A500
```

Note: the default run parameter of the compile script build.sh is "A300". It will compile Atlas300 program when running following command:
```bash
./build.sh
```

## Execution

```bash
cd out
./ObjectDetection -h

ObjectDetection [OPTION]
Options:

    -h                             Print a usage message.
    -i '<path>'                    Optional. Specify the input file: jpg iamge or H264 Video, default: test.jpeg
    -g '<graph file>'              Optional. Specify the graph config file, default: graph.config
    -m '<model file>'              Optional. Specify the model file, default: load from graph.config
    -c '<chanel number in graph>'  Optional. Specify the number of video channle in one graph, range [1, 4], default: 1
    -s '<device id>'               start chip id, default is 0
    -e '<device id>'               end chip id, default is 0
    -t '<model id>'                choose the detection type, 0: SSD, 1: YOLOV3, default is 1

Eg: ./ObjectDetection -i ./test.jpg -t 1 -m ./ObjectDetection.om -g ./graph.config -s 0 -e 1

```
#### Node
If you use SSDDetection as the target detection model, you need to select -t equals to 0 when running the program ./ObjectDetection, another need to select -t equals to 1.
