#ifndef __YOLOV3POST_H__
#define __YOLOV3POST_H__
#include <algorithm>
#include <vector>

// #define DEBUG 1          // 是否打印调试变量
// #define WITH_LANDMARK 1          // 是否有关键点分支

enum YoloType {
    YOLOV3_TINY = 2,
    YOLOV3 = 3      // 应该是有3个分支的输出
};

struct DetectBox{
    float prob;
    int classID;
    float x;
    float y;
    float width;
    float height;
    float landmark[10];
};

std::vector<DetectBox> Yolov3DetectionOutput(
    std::vector<float*> featLayerData, 
    float* biases, 
    int biasesNum, 
    int netWidth, 
    int netHeight, 
    int imgWidth,
    int imgHeight,
    int classNum,
    float thresh = 0.25, 
    float nmsThresh = 0.45,
    YoloType yoloType = YOLOV3, 
    bool usePad = false
);

std::vector<DetectBox> DbfaceDetectionOutput(
    std::vector<float*> featLayerData, 
    int netWidth, 
    int netHeight, 
    int imgWidth,
    int imgHeight,
    int classNum,
    float thresh = 0.25, 
    float nmsThresh = 0.45
);

#endif
