#include "yolov3post.h"
#include "fastmath.h"
#include <string>
#include <vector>
#include "error_code.h"
#include "hiaiengine/log.h"

const int dbface_scale = 4;

using OutputLayer = struct {
    int layerIdx;
    int width;
    int height;
    float anchors[32];
} ;

using NetInfo = struct {
    int anchorDim;
    int classNum;
    int bboxDim;
    int netWidth;
    int netHeight;
    std::vector<OutputLayer> outputLayers;
} ;


float Exp(float v)
{
    float gate = 1;
    if (std::abs(v) < gate)
        return v * std::exp(gate);

    if (v > 0)
        return std::exp(v);
    else
        return -std::exp(-v);
}



void InitNetInfo(NetInfo& netInfo, 
                 float *biases, 
                 int biasesNum, 
                 int netWidth, 
                 int netHeight, 
                 int classNum, 
                 YoloType yoloType)
{
    netInfo.anchorDim = biasesNum / static_cast<int>(yoloType) / 2;         // 3
    netInfo.bboxDim = 4;
    netInfo.classNum = classNum;
    netInfo.netWidth = netWidth;
    netInfo.netHeight = netHeight;

    int featLayerNum = static_cast<int>(yoloType);      // 3个分支的输出
    for (int i = 0; i < featLayerNum; ++i) {
        const int SCALE = 32 >> i;                      // 32, 16, 8;3个分支下采样的尺度
        OutputLayer outputLayer = {i, netWidth / SCALE, netHeight / SCALE, };
        int startIdx = (featLayerNum - 1 - i) * netInfo.anchorDim * 2;      // 12  6   0
        int endIdx = startIdx + netInfo.anchorDim * 2;                      // 18  12  6
        int idx = 0;
        for (int j = startIdx; j < endIdx; ++j) {
            outputLayer.anchors[idx++] = biases[j];     // anchors都是0->6，只赋值了前6个
        }
        netInfo.outputLayers.push_back(outputLayer);    //
    }
}

float BoxIou(DetectBox a, DetectBox b) 
{
    float left = std::max(a.x - a.width / 2.f, b.x - b.width / 2.f);
    float right = std::min(a.x + a.width / 2.f, b.x + b.width / 2.f);
    float top = std::max(a.y - a.height / 2.f, b.y - b.height / 2.f);
    float bottom = std::min(a.y + a.height / 2.f, b.y + b.height / 2.f);
    if (top > bottom || left > right) {
        return 0.0f;
    } else {
        float area = (right - left) * (bottom - top);
        return area / (a.width * a.height + b.width * b.height - area);
    }
}


void CorrectBbox(std::vector<DetectBox>& detBoxes, int netWidth, int netHeight, int imWidth, int imHeight, bool usePad)
{
    if (!usePad) return;

    // correct box
    int newWidth;
    int newHeight;
    if ((static_cast<float>(netWidth) / imWidth) < (static_cast<float>(netHeight) / imHeight)) {
        newWidth = netWidth;
        newHeight = (imHeight * netWidth) / imWidth;
    } else {
        newHeight = netHeight;
        newWidth = (imWidth * netHeight) / imHeight;
    }

    for (auto& item : detBoxes){
        item.x = (item.x * netWidth - (netWidth - newWidth) / 2.f) / newWidth;
        item.y = (item.y * netHeight - (netHeight - newHeight) / 2.f) / newHeight;
        item.width *= static_cast<float>(netWidth) / newWidth;
        item.height *= static_cast<float>(netHeight) / newHeight;
    }

    #ifdef WITH_LANDMARK
        for (auto& item: detBoxes){
            for (int i = 0; i < 5; i++){
                item.landmark[i*2+0] = (item.landmark[i * 2 + 0] * netWidth - (netWidth - newWidth) / 2.f) / newWidth;
                item.landmark[i*2+1] = (item.landmark[i * 2 + 1] * netHeight - (netHeight - newHeight) / 2.f) / newHeight;
            }
        }
    #endif
}


void NmsSort(std::vector<DetectBox>& detBoxes, int classNum, float nmsThresh)
{
    if (nmsThresh <= 0) return;

    std::vector<DetectBox> sortBoxes;
    std::vector<std::vector<DetectBox>> resClass;
    resClass.resize(classNum);

    for (const auto& item : detBoxes) {
        resClass[item.classID].push_back(item);     // 分类别装入容器
    }

    for (int i = 0; i < classNum; ++i) {
        auto& dets = resClass[i];
        if (dets.size() == 0) continue;

        std::sort(dets.begin(), dets.end(), [=](const DetectBox& a, const DetectBox& b) {       // 根据prob降序排列
            return a.prob > b.prob;
        });

        for (unsigned int m = 0;m < dets.size() ; ++m) {
            auto& item = dets[m];
            sortBoxes.push_back(item);
            for (unsigned int n = m + 1;n < dets.size() ; ++n) {
                if (BoxIou(item, dets[n]) > nmsThresh) {
                    dets.erase(dets.begin() + n);
                    --n;
                }
            }
        }
    }

    detBoxes = std::move(sortBoxes);    // 调用移动构造函数，掏空sortBoxes，掏空后，最好不要使用str
}


void GenerateBbox(std::vector<float *> featLayerData, NetInfo info, float thresh, std::vector<DetectBox>& detBoxes)
{
    for (const auto& layer : info.outputLayers) {
        int stride = layer.width * layer.height;                // 分支输出尺寸
        const float *netout = featLayerData[layer.layerIdx];    // 第一个分支
        for (int j = 0; j < stride; ++j) {                      // 每个点
            for (int k = 0; k < info.anchorDim; ++k) {          // anchorDim：3
                int bIdx = (info.bboxDim + 1 + info.classNum) * stride * k + j; // begin index // yolov3的输出（4+1+80）× 3 × w × h
                int oIdx = bIdx + info.bboxDim * stride; // objectness index // 物体置信度

                // check obj
                float objectness = fastmath::sigmoid(netout[oIdx]);
                if (objectness <= thresh)               // 物体置信度
                    continue;

                // classNum
                int classID = -1;
                float maxProb = thresh;
                float classProb;
                for (int c = 0; c < info.classNum; ++c) {   // 是哪个类别
                    classProb = fastmath::sigmoid(netout[bIdx + (info.bboxDim +  1 + c) * stride]) * objectness;
                    if (classProb > maxProb) {
                        maxProb = classProb;
                        classID = c;
                    }
                }
                if (classID >= 0) {
                    DetectBox det;
                    int row = j / layer.width;          // 物体中心的索引， 这个才是y方向
                    int col = j % layer.width;          // x方向

                    // Location
                    det.x = (col + fastmath::sigmoid(netout[bIdx])) / layer.width;      // yolov3的输出是相对于中心点的偏差，再归一化
                    det.y = (row + fastmath::sigmoid(netout[bIdx + stride])) / layer.height;
                    det.width  = fastmath::exp(netout[bIdx + 2 * stride]) * layer.anchors[2 * k] / info.netWidth;       // exp(pred_w) * anchor[0] / width
                    det.height = fastmath::exp(netout[bIdx + 3 * stride]) * layer.anchors[2 * k + 1] / info.netHeight;  // exp(pred_h) * anchor[1] / height
                    det.classID = classID;              // 类别 
                    det.prob = maxProb;                 // 置信度

                    #ifdef DEBUG
                    HIAI_ENGINE_LOG(APP_INFO, "[yolov3 process] | prob:%f, class:%d, x:%f, y:%f, w:%f, h:%f ", 
                                                            maxProb, classID, det.x, det.y, det.width, det.height);
                    #endif 

                    detBoxes.emplace_back(det);
                }
            }
        }
    }
}

void GetRealBox(std::vector<DetectBox>& detBoxes, int imgWidth, int imgHeight)
{
    for (auto& dBox : detBoxes) {
        dBox.x *= imgWidth;
        dBox.y *= imgHeight;
        dBox.width *= imgWidth;
        dBox.height *= imgHeight;
    }
}


std::vector<DetectBox> Yolov3DetectionOutput(std::vector<float *> featLayerData, 
                                             float *biases, 
                                             int biasesNum, 
                                             int netWidth, 
                                             int netHeight, 
                                             int imgWidth,
                                             int imgHeight,
                                             int classNum, 
                                             float thresh, 
                                             float nmsThresh,
                                             YoloType yoloType, 
                                             bool usePad) 
{
    static NetInfo netInfo;
    if (netInfo.outputLayers.empty()) {
        InitNetInfo(netInfo, biases, biasesNum, netWidth, netHeight, classNum, yoloType);       // biases：anchor尺寸 | netwidth：模型输入尺寸 | classNum：80 | yoloType：3 一个点3个anchor
    }

    std::vector <DetectBox> detBoxes;       // 结果容器
    GenerateBbox(featLayerData, netInfo, thresh, detBoxes);     // featLayerData：前向推理结果数据，netinfo：网络结构等参数，thresh：置信度，detBoxes：检测结果
    CorrectBbox(detBoxes, netWidth, netHeight, imgWidth, imgHeight, usePad);        // 为啥要矫正操作？
    NmsSort(detBoxes, classNum, nmsThresh); // 非极大值抑制

    return detBoxes;
}


void DbfaceGenerateBox(std::vector<float*> featLayerData, int netWidth, int netHeight, int classNum, float thresh, std::vector<DetectBox>& detBoxes)
{
    const float* class_ = featLayerData[0];      // centernet ctdet 有2个输出分支
    const float* box_ = featLayerData[1];        // box
    #ifdef WITH_LANDMARK
        const float* landmark_ = featLayerData[2];   // 关键点
    #endif
    int idx;
    netHeight /= dbface_scale;          // 网络输出的尺寸
    netWidth /= dbface_scale;           //
    int stride = netWidth * netHeight;

    int class_id;
    float class_prob, x1, y1, x2, y2, land_x, land_y, w, h;

    #ifdef DEBUG
        HIAI_ENGINE_LOG(APP_INFO, "[db process] | thresh:%f, outw:%d, outh:%d, classnum:%d, branch size:%d", 
                                  thresh, netWidth, netHeight, classNum, featLayerData.size());
    #endif

    for (int i = 0; i < classNum; i++)
    {
        for (int j = 0; j < netHeight; j++)
        {
            for (int k = 0; k < netWidth; k++)
            {
                idx = j * netWidth + k;
                class_prob = fastmath::sigmoid(class_[i * stride + idx]);
                // class_prob = class_[i * stride + idx];
                
                if (class_prob < thresh)       // 剔除小于阈值
                    continue;

                DetectBox det;
                det.classID = i;            // 类别
                det.prob = class_prob;     // 置信度

                w = box_[0 * stride + idx];     // box偏值
                h = box_[1 * stride + idx];

                // 以下两种方式都可以
                // x1 = (k + 0.5 - w / 2) * dbface_scale;       // cx - predx, 左上
                // y1 = (j + 0.5 - h / 2) * dbface_scale;       // cy - predy
                // x2 = (k + 0.5 + w / 2) * dbface_scale;       // cx + predx, 右下
                // y2 = (j + 0.5 + h / 2) * dbface_scale;       // cy + predy
                // det.x = (float)(x2 + x1) / 2 / (netWidth * dbface_scale);      // box中心x， 归一化
                // det.y = (float)(y2 + y1) / 2 / (netHeight * dbface_scale);     // box中心y， 归一化
                // det.width = (float)(x2 - x1) / (netWidth * dbface_scale);      // box宽度，  归一化
                // det.height = (float)(y2 - y1) / (netHeight * dbface_scale);    // box高度，  归一化
                det.x = (float)k / netWidth;
                det.y = (float)j / netHeight;
                det.width = (float)w / netWidth;
                det.height = (float)h / netHeight;

                #ifdef DEBUG
                    HIAI_ENGINE_LOG(APP_INFO, "[db process] | prob:%f, class:%d, x:%f, y:%f, w:%f, h:%f, cx:%d, cy:%d ", 
                                                            class_prob, i, det.x, det.y, det.width, det.height, k, j);
                #endif 

                #ifdef WITH_LANDMARK
                    for (size_t m = 0; m < 5; m++)
                    {
                        land_x = landmark_[m * stride + idx];
                        land_x = Exp(land_x) * dbface_scale;
                        land_y = landmark_[(m + 5) * stride + idx];
                        land_y = Exp(land_y) *dbface_scale;
                        det.landmark[m * 2 + 0] = (land_x + k) * dbface_scale / (netWidth * dbface_scale);   // 人脸关键点， 归一化
                        det.landmark[m * 2 + 1] = (land_y + j) * dbface_scale / (netHeight * dbface_scale);
                    }
                #endif

                detBoxes.push_back(det);        // 添加结果
            }
            
        }
        
    }
    
}


std::vector<DetectBox> DbfaceDetectionOutput(
                            std::vector<float*> featLayerDate,
                            int netWidth,
                            int netHeight,
                            int imgWidth,
                            int imgHeight,
                            int classNum,
                            float thresh,
                            float nmsThresh)        // 定义的时候不带=号
{
    std::vector<DetectBox> detBoxes;
    DbfaceGenerateBox(featLayerDate, netWidth, netHeight, classNum, thresh, detBoxes);      // 得到检测结果
    #ifdef DEBUG
        HIAI_ENGINE_LOG(APP_INFO, "[db process] | upper thresh size:%d, netw:%d, neth:%d, classnum:%d", detBoxes.size(), netWidth, netHeight, classNum);
    #endif
    CorrectBbox(detBoxes, netWidth, netHeight, imgWidth, imgHeight, true);      // 结果进行矫正，可能是jpg解码带来的影响
    NmsSort(detBoxes, classNum, nmsThresh);     // 非极大值抑制
    #ifdef DEBUG
        HIAI_ENGINE_LOG(APP_INFO, "[db process] | after nms box size:%d", detBoxes.size());
        for (auto item:detBoxes)
        {
            HIAI_ENGINE_LOG(APP_INFO, "[db process] | after nms, class:%d , prob:%f", item.classID, item.prob);
        }
    #endif

    return detBoxes;
}
