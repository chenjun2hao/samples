/**
 * ============================================================================
 *
 * Copyright (C) 2019, Huawei Technologies Co., Ltd. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1 Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   2 Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   3 Neither the names of the copyright holders nor the names of the
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * ============================================================================
 */
#include "ObjectDetectionEngine.h"
#include "engine_tools.h"
#include "error_code.h"
#include "hiaiengine/ai_memory.h"
#include "hiaiengine/c_graph.h"
#include "hiaiengine/data_type.h"
#include "hiaiengine/log.h"
#include <memory>
#include <ctime>

using std::map;
using std::shared_ptr;
using std::string;
using std::vector;

HIAI_REGISTER_DATA_TYPE("StreamInfo", StreamInfo);
HIAI_REGISTER_DATA_TYPE("ImageInfo", ImageInfo);
HIAI_REGISTER_DATA_TYPE("DetectInfo", DetectInfo);
HIAI_REGISTER_DATA_TYPE("DeviceStreamData", DeviceStreamData);

/* Read the input size of model from om file, for resize the input image in the DecodeEngine */
int g_detectInputWidth = 0;
int g_detectInputHeight = 0;
static const float THRESH = 0.5;
static const int32_t COL_SIZE = 7;
static const float COORDINATE_PARAM = 2.0;


HIAI_StatusT ObjectDetectionEngine::Init(const hiai::AIConfig &config,                                  // 在host端的main.c中的hiai::Graph::CreateGraph(graphConfigList)处初始化该类
                                         const std::vector<hiai::AIModelDescription> &model_desc)       // 检测类engine的初始化， config，model_desc从哪里传过来的。
{
    HIAI_ENGINE_LOG(APP_INFO, "[ObjectDetectionEngine] start init!");
    HIAI_StatusT ret;
    // init ai model manager
    if (modelManager == nullptr) {
        modelManager = std::make_shared<hiai::AIModelManager>();
    }
    hiai::AIModelDescription modelDesc;
    loadModelDescription(config, modelDesc);
    // init ai model manager
    ret = modelManager->Init(config, { modelDesc });        // 初始化ai model manager
    if (hiai::SUCCESS != ret) {
        HIAI_ENGINE_LOG(APP_ERROR, "ai model manager init failed!");
        return HIAI_ERROR;
    }
    // get info of input and output
    std::vector<hiai::TensorDimension> inputTensorDims;
    std::vector<hiai::TensorDimension> outputTensorDims;
    ret = modelManager->GetModelIOTensorDim(modelDesc.name(), inputTensorDims, outputTensorDims);       // 从模型中得到input/output tensor的信息
    if (ret != hiai::SUCCESS) {
        HIAI_ENGINE_LOG(APP_ERROR, "[ObjectDetectionEngine] GetModelIOTensorDim() failed.");
        return HIAI_ERROR;
    }
    // Get input image size form om file
    kHeight = inputTensorDims[0].h;         // 为啥加个 0 ，第一个输入tensor ？？？
    kWidth = inputTensorDims[0].w;
    kChannel = inputTensorDims[0].c;
    kBatchSize = inputTensorDims[0].n;


    kAlignedWidth = ALIGN_UP(kWidth, ALIGN_16);     // 16/2对齐
    kAlignedHeight = ALIGN_UP(kHeight, ALIGN_2);

    kInputSize = kBatchSize * kAlignedHeight * kAlignedWidth * YUV_BYTES;
    // get ride off reading nchw from graph.config
    // we can get from input TensorDimensions, and the alignments size are different between the objection detection model of yolov3 and ssd.
    if (kInputSize != inputTensorDims[0].size) {
        
        kAlignedWidth = ALIGN_UP(kWidth, ALIGN_128);    // 128/16对齐
        kAlignedHeight = ALIGN_UP(kHeight, ALIGN_16);
        kInputSize = kBatchSize * kAlignedHeight * kAlignedWidth * YUV_BYTES;
        if (kInputSize != inputTensorDims[0].size) {

            HIAI_ENGINE_LOG(APP_ERROR, "[ObjectDetectionEngine] inputSize != inputTensorDims[0].size (%d vs. %d)", kInputSize, inputTensorDims[0].size);
            return HIAI_ERROR;
        }
    }

    // Read the input size of model from om file, for resize the input image in the DecodeEngine 
    g_detectInputWidth = kWidth;
    g_detectInputHeight = kHeight;

    for (auto &dims : inputTensorDims) {
        logDumpDims(dims);                  // @@@重要， 显示输入tensor的维度信息
    }
    for (auto &dims : outputTensorDims) {   // @@@重要， 显示输出tensor的维度信息
        logDumpDims(dims);
    }

    // pre allocate input data buffer
    if (inputTensorDims.size() != 1) {
        HIAI_ENGINE_LOG(APP_ERROR, "[ObjectDetectionEngine] inputTensorDims.size() != 1");
        return HIAI_ERROR;
    }
    std::shared_ptr<uint8_t> inPtr(static_cast<uint8_t *>(HIAI_DVPP_DMalloc(kInputSize)), HIAI_DVPP_DFree);
    inputDataBuffer = std::make_pair(inPtr, kInputSize);

    hiai::AITensorDescription tensorDesc = hiai::AINeuralNetworkBuffer::GetDescription();
    std::shared_ptr<hiai::IAITensor> inputTensor = hiai::AITensorFactory::GetInstance()->CreateTensor(tensorDesc,
                                                   static_cast<void *>(inPtr.get()), kInputSize);
    inputTensorVec.push_back(inputTensor);      // 声明输入变量的内存

    // pre allocate output data buffer
    for (uint32_t index = 0; index < outputTensorDims.size(); index++) {        // 有几个输出
        hiai::AITensorDescription outputTensorDesc = hiai::AINeuralNetworkBuffer::GetDescription();
        HIAI_ENGINE_LOG(APP_INFO, "outputTensorDims[index].size %d", outputTensorDims[index].size);

        uint8_t *buf = static_cast<uint8_t *>(HIAI_DVPP_DMalloc(outputTensorDims[index].size));     // 为输出声明内存
        if (buf == nullptr) {
            HIAI_ENGINE_LOG(APP_ERROR, "[ObjectDetectionEngine] HIAI_DVPP_DMalloc failed.");
            return HIAI_ERROR;
        }
        outputDataBuffer.push_back(std::shared_ptr<uint8_t>(buf, HIAI_DVPP_DFree));         // 和outputTensorVec的区别是？？？

        std::shared_ptr<hiai::IAITensor> outputTensor = hiai::AITensorFactory::GetInstance()->CreateTensor( \
                                                        outputTensorDesc, buf, outputTensorDims[index].size);
        shared_ptr<hiai::AINeuralNetworkBuffer> nn_tensor = static_pointer_cast<hiai::AINeuralNetworkBuffer>( \
                                                            outputTensor);
        nn_tensor->SetName(outputTensorDims[index].name);
        outputTensorVec.push_back(outputTensor);
    }

    /* create the vpc object */
    if (piDvppApiVpc == NULL) {
        ret = CreateDvppApi(piDvppApiVpc);
        if ((ret != HIAI_OK) && (piDvppApiVpc == NULL)) {
            HIAI_ENGINE_LOG(APP_ERROR, "ObjectDetectionEngine fail to intialize vpc api!");
            return HIAI_ERROR;
        }
    }

    HIAI_ENGINE_LOG(APP_INFO, "ObjectDetectionEngine initial successfully!");
    return HIAI_OK;
}


ObjectDetectionEngine::~ObjectDetectionEngine()
{
    /* Destroy vpc object */
    if (piDvppApiVpc != NULL) {
        HIAI_ENGINE_LOG(APP_INFO, "Destroy vpc api!");
        DestroyDvppApi(piDvppApiVpc);
        piDvppApiVpc = NULL;
    }
}


HIAI_IMPL_ENGINE_PROCESS("ObjectDetectionEngine", ObjectDetectionEngine, OBJECT_DETECT_INPUT_SIZE)          // objectdectectionEngine初始化
{
    clock_t time_start = clock();       //  前向+后处理计时
    HIAI_ENGINE_LOG(APP_INFO, "ObjectDetectionEngine Process start");
    HIAI_StatusT ret = HIAI_OK;
    std::shared_ptr<DeviceStreamData> inputArg;
    if (nullptr != arg0) {
        inputArg = std::static_pointer_cast<DeviceStreamData>(arg0);        // 接收数据
        if (inputArg->info.isEOS == 1) {
            HIAI_ENGINE_LOG(APP_INFO, "[ObjectDection] Video or jpg file end, clean up the buffer!");
        } else {
            inputArgQueue.push_back(inputArg);
            if (inputArgQueue.size() < kBatchSize) {
                HIAI_ENGINE_LOG(APP_INFO,
                                "Collecting batch data, in current, queue size %d", inputArgQueue.size());
                return HIAI_OK;
            }
        }
    } else {
        if (inputArgQueue.size() <= 0) {
            HIAI_ENGINE_LOG(APP_ERROR, "ObjectDetectionEngine get invalid input!");
            return HIAI_ERROR;
        }
        HIAI_ENGINE_LOG(APP_INFO, "inputArgQueue overtime!");
    }

    uint32_t originWidth = 0;
    uint32_t originHeight = 0;
    // resize yuv data to input size
    uint8_t *dataBufferPtr = inputDataBuffer.first.get();       // init中声明的内存
    int buffLen = kInputSize;
    for (int i = 0; i < inputArgQueue.size(); i++) {
        std::shared_ptr<DeviceStreamData> temp = inputArgQueue[i];      // 第几个输入
        errno_t ret = memcpy_s(dataBufferPtr, buffLen, temp->detectImg.buf.data.get(),      // 拷贝
                               temp->detectImg.buf.len_of_byte);
        // 2020-06-19
        HIAI_ENGINE_LOG(APP_INFO, "[input] input image size:%d", temp->detectImg.buf.len_of_byte);
    
        if (ret != EOK) {
            HIAI_ENGINE_LOG(APP_ERROR, "memcpy_s of ObjectDetectionEngine is wrong");
            return HIAI_ERROR;
        }

        dataBufferPtr += temp->detectImg.buf.len_of_byte;
        buffLen -= temp->detectImg.buf.len_of_byte;

        originWidth = temp->imgOrigin.width;
        originHeight = temp->imgOrigin.height;

        // 2020-06-19
        HIAI_ENGINE_LOG(APP_INFO, "input image w:%d | h:%d | inputargsize:%d", originWidth, originHeight, inputArgQueue.size());
    }

    // inference // 
    HIAI_ENGINE_LOG(APP_INFO, "AI Model Manager Process Start!");
    hiai::AIContext aiContext;
    ret = modelManager->Process(aiContext, inputTensorVec, outputTensorVec, 0);     // 上面的dataBufferPtr在inputTensorVec中 // 无法看到源码与过程
    if (hiai::SUCCESS != ret) {
        HIAI_ENGINE_LOG(APP_ERROR, "AI Model Manager Process failed");
        return HIAI_ERROR;
    }
    HIAI_ENGINE_LOG(APP_INFO, "AI Model Manager Process Finished!");

    // Two model are available.
    HIAI_ENGINE_LOG(APP_INFO, "[OBJECTDETECTIONMODELTYPE] inputArg->modelType: %d", inputArg->modelType);
	
    // 模型后处理
    if (inputArg->info.isEOS != 1) {
        if (inputArg->modelType == 0) {
            PostProcessDetectionSSD();                      // SSD后处理
        } 
        else if (inputArg->modelType == 1)
        {
            ResizeInfo resizeInfo;
            resizeInfo.originWidth = originWidth;   // 图片的原尺寸 w
            resizeInfo.originHeight = originHeight; // 图片的原尺寸 h
            resizeInfo.resizeWidth = kWidth;        // 网络模型的输入尺寸w
            resizeInfo.resizeHeight = kHeight;      // 网络模型的输入尺寸h

            PostProcessDetectionYoloV3(resizeInfo);  // yolov3 后处理
        }
        else if (inputArg->modelType == 2)
        {
            ResizeInfo resizeInfo;
            resizeInfo.originWidth = originWidth;   // 图片的原尺寸 w
            resizeInfo.originHeight = originHeight; // 图片的原尺寸 h
            resizeInfo.resizeWidth = kWidth;        // 网络模型的输入尺寸w
            resizeInfo.resizeHeight = kHeight;      // 网络模型的输入尺寸h

            
            PostProcessDetectionDbface(resizeInfo); // dbface 后处理
            clock_t time_end = clock();

            #ifdef DEBUG
                HIAI_ENGINE_LOG(APP_INFO, "[centernet post process] | time:%f ms, end:%d, start:%d, cha:%d", 
                1000 * (double)(time_end - time_start) / (double)CLOCKS_PER_SEC, time_end, time_start, time_end-time_start);    // ms单位
            #endif
        }
    }

    // send data to next engine // 将检测结果数据回传到host端 // detectResult
    for (int32_t j = 0; j < inputArgQueue.size(); j++) {
        std::shared_ptr<DeviceStreamData> deviceStreamData = inputArgQueue[j];
        if (deviceStreamData->detectResult.size() > 0) {
            // crop small image for follow up process
            if (HIAI_OK != hiai::Engine::SendData(0, "DeviceStreamData",
                                                  std::static_pointer_cast<void>(deviceStreamData))) {
                HIAI_ENGINE_LOG(APP_ERROR, "ObjectDetectionEngine senddata error!");
                return HIAI_ERROR;
            }
            HIAI_ENGINE_LOG(APP_INFO, "ObjectDetectionEngine senddata successfully!");
        }
    }

    // clear the inputArgQueue 
    inputArgQueue.clear();

    // send // 发送结果数据
    if (inputArg->info.isEOS == 1) {
        std::shared_ptr<DeviceStreamData> deviceStreamData = std::make_shared<DeviceStreamData>();
        deviceStreamData->info = inputArg->info;
        if (HIAI_OK != hiai::Engine::SendData(0, "DeviceStreamData",
                                              std::static_pointer_cast<void>(deviceStreamData))) {
            HIAI_ENGINE_LOG(APP_ERROR, "ObjectDetectionEngine senddata error!");
            return HIAI_ERROR;
        }
        HIAI_ENGINE_LOG(APP_INFO, "[ObjectDection] Video end, clean up the buffer, senddata flag !");
    }

    return HIAI_OK;     // yolov3的推理结束
}

enum SSD_INDEX {BATCH_INDEX = 0, LABEL_INDEX = 1, SCORE_INDEX = 2, \
                XMIN_INDEX = 3, YMIN_INDEX = 4, XMAX_INDEX = 5, YMAX_INDEX = 6};

HIAI_StatusT ObjectDetectionEngine::PostProcessDetectionSSD(void)
{
    /*
    * tensor shape 200x7x1x1
    * for each row (7 elements), layout as follows
    * batch, label, score, xmin, ymin, xmax, ymax
    */
    shared_ptr<hiai::AINeuralNetworkBuffer> tensorResults = std::static_pointer_cast<hiai::AINeuralNetworkBuffer>
                                                            (outputTensorVec[0]);
    shared_ptr<hiai::AINeuralNetworkBuffer> tensorFaceNum = std::static_pointer_cast<hiai::AINeuralNetworkBuffer>
                                                            (outputTensorVec[1]);
    float *resStartPtr = static_cast<float *>(tensorResults->GetBuffer());
    // Don't use inputArgQueue.size() to instead of kBatchSize
    int numPerBatch = tensorResults->GetSize() / sizeof(float) / kBatchSize;
    float *validNumList = static_cast<float *>(tensorFaceNum->GetBuffer());
    if ((resStartPtr == NULL) || (validNumList == NULL)) {
        HIAI_ENGINE_LOG(APP_ERROR, "Result of inference is Null!");
        return HIAI_ERROR;
    }

    int validObjectCount = 0;
    for (int32_t j = 0; j < inputArgQueue.size(); j++) {
        HIAI_ENGINE_LOG(APP_INFO, "Numder of detected faces %d", (int32_t)validNumList[j]);
        std::shared_ptr<DeviceStreamData> deviceStreamData = inputArgQueue[j];
        float *resPtr = NULL;
        resPtr = resStartPtr + numPerBatch * j;

        /* parse the single batch result */
        validObjectCount = 0;
        for (int32_t i = 0; i < (int32_t)validNumList[j]; i++) {
            DetectInfo detectInfo;
            int32_t batch = (int32_t)(*resPtr);
            detectInfo.classId = (int32_t)(*(resPtr + LABEL_INDEX));
            detectInfo.confidence = *(resPtr + SCORE_INDEX);
            detectInfo.location.anchor_lt.x = (int32_t)(*(resPtr + XMIN_INDEX) * deviceStreamData->imgOrigin.width);
            detectInfo.location.anchor_lt.y = (int32_t)(*(resPtr + YMIN_INDEX) * deviceStreamData->imgOrigin.height);
            detectInfo.location.anchor_rb.x = (int32_t)(*(resPtr + XMAX_INDEX) * deviceStreamData->imgOrigin.width);
            detectInfo.location.anchor_rb.y = (int32_t)(*(resPtr + YMAX_INDEX) * deviceStreamData->imgOrigin.height);
            if ((detectInfo.confidence > THRESH) && (detectInfo.classId > 0)) {
                validObjectCount++;
                HIAI_ENGINE_LOG(APP_INFO, "batch %d, label %d, score %f, xmin %d, ymin %d, xmax %d, ymax %d",
                                batch, detectInfo.classId, detectInfo.confidence,
                                detectInfo.location.anchor_lt.x, detectInfo.location.anchor_lt.y,
                                detectInfo.location.anchor_rb.x, detectInfo.location.anchor_rb.y);
                /* output detect information */
                deviceStreamData->detectResult.push_back(detectInfo);
            }
            resPtr += COL_SIZE;
        }
        HIAI_ENGINE_LOG(APP_INFO, "validObjectCount %d", validObjectCount);
    }

    return HIAI_OK;
}

HIAI_StatusT ObjectDetectionEngine::PostProcessDetectionYoloV3(ResizeInfo& resizeInfo)
{
    uint32_t all_size = 0;
    uint32_t single_size = 0;
    int batch = kBatchSize;
    if (!batch) {
        batch = 1;
    }
    HIAI_StatusT ret = HIAI_OK;
    for (int j = 0; j < outputTensorVec.size(); j++) {          // outputTensorVec前向推理的结果
        std::shared_ptr<hiai::AISimpleTensor> result_tensor = std::static_pointer_cast<hiai::AISimpleTensor>(outputTensorVec[j]);
        all_size += result_tensor->GetSize();
        single_size += result_tensor->GetSize() / batch;
        HIAI_ENGINE_LOG(APP_INFO, "all: %d, single: %d", all_size, single_size);
    }

    uint32_t idx = 0;
    uint32_t offset = 0;
    int box_num = 0;
    int all_box_num = 0;

    for (int i = 0; i < inputArgQueue.size(); i++) {        // inputArgQueue.size()只有1.
        idx = 0;
        std::vector<float*> singleResult;
        for (int j = 0; j < outputTensorVec.size(); j++) {  // 有3个分支的输出
            std::shared_ptr<hiai::AISimpleTensor> resultTensor = std::static_pointer_cast<hiai::AISimpleTensor>(outputTensorVec[j]);
            size_t offset = i * resultTensor->GetSize() / batch;
            // 2020-06-19
            #ifdef DEBUG
                HIAI_ENGINE_LOG(APP_INFO, "[post process] | branch:%d, buffer size:%d, tensor size:%d", j, offset, resultTensor->GetSize());
            #endif

            singleResult.push_back(reinterpret_cast<float*>(reinterpret_cast<uint8_t*>(resultTensor->GetBuffer()) + offset));       // 结果数据
            idx += resultTensor->GetSize() / batch;
        }
        
        auto detectBox = Yolov3DetectionOutput(singleResult, g_biases, BIASES_NUM, resizeInfo.resizeWidth, resizeInfo.resizeHeight,         // resizeWidth， resizeHeight是模型的输入尺寸
		                                       resizeInfo.originWidth, resizeInfo.originHeight, CLASS_NUM, 0.25, 0.45, YOLOV3, true);       // yolov3 后处理
        if (detectBox.size() == 0) {
            HIAI_ENGINE_LOG(APP_ERROR, "detectBox is NULL!");
            singleResult.clear();
            detectBox.clear();
            return HIAI_ERROR;
        }
        
        std::shared_ptr<DeviceStreamData> deviceStreamData = inputArgQueue[i];

        // parse the single batch result, We need to filter out values that exceed the range of the image.
        int validObjectCount = 0;
        for (int k = 0; k < detectBox.size(); k++) {
            DetectInfo detectInfo;
            detectInfo.classId = detectBox[k].classID;
            detectInfo.confidence = detectBox[k].prob;
            detectInfo.location.anchor_lt.x = (detectBox[k].x - detectBox[k].width / COORDINATE_PARAM > 0) ? (int32_t)((detectBox[k].x - detectBox[k].width / COORDINATE_PARAM) * deviceStreamData->imgOrigin.width) : 0;
            detectInfo.location.anchor_lt.y = (detectBox[k].y - detectBox[k].height / COORDINATE_PARAM > 0) ? (int32_t)((detectBox[k].y - detectBox[k].height / COORDINATE_PARAM) * deviceStreamData->imgOrigin.height) : 0;
            detectInfo.location.anchor_rb.x = (int32_t)((detectBox[k].x + detectBox[k].width / COORDINATE_PARAM) * deviceStreamData->imgOrigin.width);
            detectInfo.location.anchor_rb.y = (int32_t)((detectBox[k].y + detectBox[k].height / COORDINATE_PARAM) * deviceStreamData->imgOrigin.height);
            if ((detectInfo.confidence > THRESH) && (detectBox[k].classID >= 0)) {
                validObjectCount++;
                deviceStreamData->detectResult.push_back(detectInfo);
            }
        }
        all_box_num += box_num;
        detectBox.clear();
    }

    detectBox.clear();
    return HIAI_OK;
}


HIAI_StatusT ObjectDetectionEngine::PostProcessDetectionDbface(ResizeInfo& resizeInfo)
{
    uint32_t all_size = 0;
    uint32_t single_size = 0;
    int batch = kBatchSize;
    if (!batch) {
        batch = 1;
    }
    HIAI_StatusT ret = HIAI_OK;
    for (int j = 0; j < outputTensorVec.size(); j++) {          // outputTensorVec前向推理的结果
        std::shared_ptr<hiai::AISimpleTensor> result_tensor = std::static_pointer_cast<hiai::AISimpleTensor>(outputTensorVec[j]);
        all_size += result_tensor->GetSize();
        single_size += result_tensor->GetSize() / batch;
        HIAI_ENGINE_LOG(APP_INFO, "all: %d, single: %d", all_size, single_size);
    }

    uint32_t idx = 0;
    uint32_t offset = 0;
    int box_num = 0;
    int all_box_num = 0;

    for (int i = 0; i < inputArgQueue.size(); i++) {        // inputArgQueue.size()只有1.
        idx = 0;
        std::vector<float*> singleResult;
        for (int j = 0; j < outputTensorVec.size(); j++) {  // 有3个分支的输出
            std::shared_ptr<hiai::AISimpleTensor> resultTensor = std::static_pointer_cast<hiai::AISimpleTensor>(outputTensorVec[j]);
            size_t offset = i * resultTensor->GetSize() / batch;
            // 2020-06-19
            #ifdef DEBUG
                HIAI_ENGINE_LOG(APP_INFO, "[centernet post process] | branch:%d, buffer size:%d, tensor size:%d", j, offset, resultTensor->GetSize());
            #endif

            singleResult.push_back(reinterpret_cast<float*>(reinterpret_cast<uint8_t*>(resultTensor->GetBuffer()) + offset));       // 结果数据
            idx += resultTensor->GetSize() / batch;
        }
        
        auto detectBox = DbfaceDetectionOutput(singleResult, resizeInfo.resizeWidth, resizeInfo.resizeHeight,
                                                resizeInfo.originWidth, resizeInfo.originHeight, 2);       // dbface 后处理; classnums=2
        if (detectBox.size() == 0) {
            HIAI_ENGINE_LOG(APP_ERROR, "detectBox is NULL!");
            singleResult.clear();
            detectBox.clear();
            return HIAI_ERROR;
        }
        
        std::shared_ptr<DeviceStreamData> deviceStreamData = inputArgQueue[i];

        // parse the single batch result, We need to filter out values that exceed the range of the image.
        int validObjectCount = 0;
        for (int k = 0; k < detectBox.size(); k++) {
            DetectInfo detectInfo;
            detectInfo.classId = detectBox[k].classID;
            detectInfo.confidence = detectBox[k].prob;
            detectInfo.location.anchor_lt.x = (detectBox[k].x - detectBox[k].width / COORDINATE_PARAM > 0) ? (int32_t)((detectBox[k].x - detectBox[k].width / COORDINATE_PARAM) * deviceStreamData->imgOrigin.width) : 0;
            detectInfo.location.anchor_lt.y = (detectBox[k].y - detectBox[k].height / COORDINATE_PARAM > 0) ? (int32_t)((detectBox[k].y - detectBox[k].height / COORDINATE_PARAM) * deviceStreamData->imgOrigin.height) : 0;
            detectInfo.location.anchor_rb.x = (int32_t)((detectBox[k].x + detectBox[k].width / COORDINATE_PARAM) * deviceStreamData->imgOrigin.width);
            detectInfo.location.anchor_rb.y = (int32_t)((detectBox[k].y + detectBox[k].height / COORDINATE_PARAM) * deviceStreamData->imgOrigin.height);
            if ((detectInfo.confidence > THRESH) && (detectBox[k].classID >= 0)) {
                validObjectCount++;
                deviceStreamData->detectResult.push_back(detectInfo);
            }
        }
        all_box_num += box_num;
        detectBox.clear();
    }

    detectBox.clear();
    return HIAI_OK;
}