/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2018-2020. All rights reserved.
 * Description: 文件功能描述
 * Author: Atlas
 * Create: 2020-03-09
 */
#ifndef ATLASFACEDEMO_UTILS_COMMON_H
#define ATLASFACEDEMO_UTILS_COMMON_H
#include <string>
#include <unordered_map>
#include <sys/stat.h>
#include "error_code.h"

typedef struct stat Stat;
using std::string;
using std::unordered_map;

inline unordered_map<string, string> kvmap(const hiai::AIConfig& config)
{
    unordered_map<string, string> kv;
    for (int index = 0; index < config.items_size(); ++index) {
        const ::hiai::AIConfigItem& item = config.items(index);
        kv.insert(std::make_pair(item.name(), item.value()));
    }
    return std::move(kv);
}

static std::vector<std::string> splitpath(
    const std::string& str, const std::set<char> delimiters)
{
    std::vector<std::string> result;
    char const* pch = str.c_str();
    char const* start = pch;
    for (; *pch; ++pch) {
        if (delimiters.find(*pch) != delimiters.end()) {
            if (start != pch) {
                std::string str(start, pch);
                result.push_back(str);
            } else {
                result.push_back("");
            }
            start = pch + 1;
        }
    }
    result.push_back(start);
    return result;
}

inline HIAI_StatusT saveFileBin(uint8_t* data_ptr, uint32_t data_len, const std::string& filename)
{
    FILE* fp = fopen(filename.c_str(), "wb");
    if (NULL == fp) {
        HIAI_ENGINE_LOG(APP_ERROR, "Save file engine: open file fail");
        fclose(fp);
        return HIAI_ERROR;
    } else {
        fwrite(data_ptr, 1, data_len, fp);
        fflush(fp);
        fclose(fp);
    }
    return HIAI_OK;
}

template <class T>
inline string to_string(T __val, uint32_t n_zeros, char pchar = '0')
{
    std::string valstr = std::to_string(__val);
    int n_pads = n_zeros - valstr.length();
    return n_pads > 0 ? std::string(n_pads, pchar) + valstr : valstr;
}

#endif  // ATLASFACEDEMO_UTILS_COMMON_H
