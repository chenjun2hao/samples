project(device)
add_compile_options(-std=c++11 -fopenmp -DCPU_ONLY -march=armv8-a -DENABLE_NEON=ON -fPIC -fstack-protector-all)

include(${PROJECT_SRC_ROOT}/../CMake/Ascend.cmake)
include_directories(${PROJECT_SRC_ROOT}/../Common/)

aux_source_directory(${PROJECT_SRC_ROOT}/../Common/CropResize CropResize_SRC)
aux_source_directory(${PROJECT_SRC_ROOT}/JpegDecode JpegDecode_SRC)
aux_source_directory(${PROJECT_SRC_ROOT}/PngDecode PngDecode_SRC)
aux_source_directory(${PROJECT_SRC_ROOT}/ObjectClassification ObjectClassification_SRC)

add_library(Device SHARED
            ${DvppJpegEncode_SRC}
            ${CropResize_SRC}
            ${JpegDecode_SRC}
            ${PngDecode_SRC}
            ${ObjectClassification_SRC}
            ${JpegEncode_SRC})

target_include_directories(Device PUBLIC
        ${PROJECT_SRC_ROOT}/../Common/CropResize
        ${PROJECT_SRC_ROOT}/JpegDecode
        ${PROJECT_SRC_ROOT}/PngDecode
        ${PROJECT_SRC_ROOT}/ObjectClassification)

target_link_libraries(Device ${DDK_DEVICE_LIBRARIES} ${DDK_DEVICE_LIBRARIES} -Wl,-z,relro,-z,now,-z,noexecstack)


