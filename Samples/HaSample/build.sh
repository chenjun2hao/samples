#!/bin/bash

path_cur=$(cd `dirname $0`; pwd)

if [ "$1" == "A500" ];then
    mkdir -p $path_cur/out
    cd $path_cur/out
    cmake ..
    make    
fi
