project(host)
add_compile_options(-std=c++11 -fPIE -fstack-protector-all)

if(${BUILD_TARGET} MATCHES "A200")
    add_definitions(-DATLAS_DEVICE)
else()
    add_definitions(-DATLAS_HOST)
endif()

# copy graph.config file
file(COPY ${PROJECT_SRC_ROOT}/graph.config
        DESTINATION ${CMAKE_BINARY_DIR})

include_directories(${PROJECT_SRC_ROOT}/../Common/GraphManager)
aux_source_directory(${PROJECT_SRC_ROOT}/../Common/GraphManager GraphManager_SRC)
include_directories(${PROJECT_SRC_ROOT}/../Common/FileManager)
aux_source_directory(${PROJECT_SRC_ROOT}/../Common/FileManager FileManager_SRC)
aux_source_directory(${PROJECT_SRC_ROOT}/SrcEngine SrcEngine_SRC)
aux_source_directory(${PROJECT_SRC_ROOT}/DstEngine DstEngine_SRC)
aux_source_directory(${PROJECT_SRC_ROOT} main_SRC)

if(${BUILD_TARGET} MATCHES "A200")
    file(GLOB DDK_HOST_LIBRARIES $ENV{DDK_HOME}/${_DDK_HOST_PATH_SUFFIXES}/*.so)
endif()

add_executable(main ${main_SRC} ${GraphManager_SRC} ${FileManager_SRC} ${SrcEngine_SRC} ${DstEngine_SRC})
target_include_directories(main PUBLIC ${DDK_PROTOBUF_INCLUDE_DIRS})
target_link_libraries(main ${DDK_HOST_LIBRARIES} ${DDK_PROTOBUF_LIBRARYS} -Wl,-z,relro,-z,now,-z,noexecstack -pie)

